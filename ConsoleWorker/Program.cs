﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ConsoleWorker.Models;
using Newtonsoft.Json;


namespace ConsoleWorker
{
    class Program
    {

        const int XFrom = 1;
        const int XTo = 200;
        const float XDelta = 0.0001f;

        static void Main(string[] args)
        {
            try
            {
                var isMain = AppSettings.Get<bool>("IsMainRole");
                var ipAddress = AppSettings.Get<string>("MainHost");
                var ipPort = AppSettings.Get<int>("MainPort");
                if (isMain)
                    RunMainWorker(ipAddress, ipPort);
                else
                    RunWorker(ipAddress, ipPort);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }

        static void RunMainWorker(string address, int port)
        {

            var server = new Server(address, port);
            try
            {
                var listener = new Thread(server.Listen);
                listener.Start();            
                server.StartCalc(XFrom, XTo, XDelta);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                server.Disconnect();
            }
        }

        static void RunWorker(string address, int port)
        {
            var worker = new Worker(address, port);
            try
            {
                worker.StartWorking();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                worker.Disconnect();
            }
        }
    }

}

