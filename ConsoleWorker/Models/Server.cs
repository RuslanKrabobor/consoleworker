﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ConsoleWorker.Models
{
    public class Server
    {
        private string _address;
        private int _port;
        private bool _mainFinish;
        TcpListener _tcpListener;
        List<Client> _workers = new List<Client>();
        List<Client> _inWork = new List<Client>();
        private float _area;


        public Server(string address, int port)
        {
            this._address = address;
            this._port = port;
        }

        public void AddWorker(Client worker)
        {
            _workers.Add(worker);
            ShowWorkersCount();
        }

        public void RemoveWorker(string id)
        {
            Client client = _workers.FirstOrDefault(c => c.Id == id);
            if (client != null)
                _workers.Remove(client);
            ShowWorkersCount();
        }

        public void ShowWorkersCount()
        {
            Console.WriteLine("Current count of _workers: {0}", WorkersCount());
        }

        public int WorkersCount()
        {
            return _workers.Count;
        }

        public void Listen()
        {
            try
            {
                _tcpListener = new TcpListener(IPAddress.Parse(_address), _port);
                _tcpListener.Start();
                Console.WriteLine("Server started. Waiting for _workers...");
                ShowWorkersCount();
                while (true)
                {
                    TcpClient tcpClient = _tcpListener.AcceptTcpClient();

                    Client clientObject = new Client(tcpClient, this);
                    Thread clientThread = new Thread(new ThreadStart(clientObject.Process));
                    clientThread.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Disconnect();
            }
        }

        public void SendCalcCommand(List<Message> msgs)
        {
            _inWork.Clear();
            for (int i = 0; i < _workers.Count; i++)
            {
                if (i > msgs.Count)
                    break;

                string serializedJson = JsonConvert.SerializeObject(msgs[i]);

                byte[] data = Encoding.Unicode.GetBytes(serializedJson);
                var worker = _workers[i];
                worker.Stream.Write(data, 0, data.Length);
                _inWork.Add(worker);
            }
        }

        public void BroadcastMessage(string message, string id)
        {
            byte[] data = Encoding.Unicode.GetBytes(message);
            foreach (var worker in _workers)
            {
                if (worker.Id != id)
                    worker.Stream.Write(data, 0, data.Length);
            }
        }

        public void Disconnect()
        {
            _tcpListener.Stop();

            foreach (var worker in _workers)
                worker.Close();

            Environment.Exit(0);
        }

        public void StartCalc(int xFrom, int xTo, float xDelta)
        {
            _mainFinish = false;
            Command cmd = EnterCommand();
            var msgs = GetMessages(xFrom, xTo, xDelta, WorkersCount(), cmd);

            var msg = msgs.FirstOrDefault();
            msgs.Remove(msg);
            SendCalcCommand(msgs);

            var calc = new Calculator(msg);
            var res = calc.CalcY();
            _area += calc.CalcArea(res);
            _mainFinish = true;
            ShowArea();
        }

        static Command EnterCommand()
        {
            int result;
            Console.WriteLine("Select an example for calculating the area:\n1 sin(x+5)\n2 sqrt(6-x)\n3 cos(x*2)");
            while (true)
            {
                var command = Console.ReadLine();

                int.TryParse(command, out result);
                if (result > 0 && result < 4)
                    break;
                else
                    Console.WriteLine("Incorrect number of example. Please choose 1, 2 or 3");
            }
            return (Command)result;
        }

        static List<Message> GetMessages(int xFrom, int xTo, float xDelta, int count, Command cmd)
        {
            var totalCount = count + 1;
            List<Message> list = new List<Message>();
            int span = (int)Math.Ceiling((double)(xTo - xFrom) / totalCount);
            var tmpFrom = xFrom;
            while (true)
            {
                var tmpTo = tmpFrom + span < xTo ? tmpFrom + span : xTo;
                list.Add(new Message { Operation = cmd, XFrom = tmpFrom, XTo = tmpTo, XDelta = xDelta });
                tmpFrom = tmpTo;
                if (tmpFrom == xTo)
                    break;
            }
            return list;
        }

        public void CalcArea(string message, Client client)
        {
            float res;
            if (float.TryParse(message, out res))
                _inWork.Remove(client);

            _area += res;
            ShowArea();
        }

        public void ShowArea()
        {
            if (_inWork.Count == 0 && _mainFinish)
            {
                Console.WriteLine("Area: {0}", _area);
            }
        }
    }
}
