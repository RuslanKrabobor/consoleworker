﻿using System;
using System.Net.Sockets;
using System.Text;

namespace ConsoleWorker.Models
{
    public class Client
    {
        public string Id { get { return userName; }}
        public NetworkStream Stream { get; private set; }
        private string userName;
        TcpClient client;
        Server server; 

        public Client(TcpClient tcpClient, Server serverObject)
        {
            client = tcpClient;
            server = serverObject;          
        }

        public void Process()
        {
            try
            {
                Stream = client.GetStream();
                userName = GetMessage();
                Console.WriteLine("Worker #{0} joined.", userName);
                server.AddWorker(this);

                while (true)
                {
                    try
                    {
                        var message = GetMessage();
                        server.CalcArea(message, this);
                        server.RemoveWorker(this.Id);
                    }
                    catch
                    {
                        Console.WriteLine("Worker #{0} left.", userName);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                server.RemoveWorker(this.Id);
                Close();
            }
        }

        private string GetMessage()
        {
            byte[] data = new byte[64]; 
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = Stream.Read(data, 0, data.Length);
                builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
            }
            while (Stream.DataAvailable);

            return builder.ToString();
        }

        protected internal void Close()
        {
            Stream?.Close();
            client?.Close();
        }
    }
}
