﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ConsoleWorker.Models
{
    public class Worker
    {
        private TcpClient client;
        private NetworkStream stream;
        private string Id;

        public Worker(string address, int port)
        {
            Id = Guid.NewGuid().ToString();
            Console.WriteLine("Hello, im worker #{0}!", Id);
            client = new TcpClient();
            while (true)
            {
                try
                {
                    client.Connect(address, port);
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Can't connect to server on {0}:{1}",address, port);
                    Thread.Sleep(10000);
                } 
            }
            stream = client.GetStream();  
        }

        public void StartWorking()
        {
            byte[] data = Encoding.Unicode.GetBytes(Id);
            stream.Write(data, 0, data.Length);
            Thread receiveThread = new Thread(ReceiveMessage);
            receiveThread.Start();
        }

        public void SendMessage(string message)
        {
            byte[] data = Encoding.Unicode.GetBytes(message);
            stream.Write(data, 0, data.Length);
        }

        public void ReceiveMessage()
        {
            while (true)
            {
                try
                {
                    byte[] data = new byte[64];
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (stream.DataAvailable);

                    string message = builder.ToString();

                    Message msg = JsonConvert.DeserializeObject<Message>(message);
                    var calc = new Calculator(msg);
                    var res = calc.CalcY();
                    var area = calc.CalcArea(res);
                    Console.WriteLine("Area: {0}", area);
                    SendMessage(area.ToString());
                }
                catch
                {
                    Console.WriteLine("Connection error!");
                    Console.ReadLine();
                    Disconnect();
                }
            }
        }

        public void Disconnect()
        {
            stream?.Close();
            client?.Close();
        }    
    }
}
