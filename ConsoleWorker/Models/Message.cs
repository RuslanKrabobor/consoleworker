﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleWorker.Models
{
    [Serializable]
    public class Message
    {
        public int XFrom;

        public int XTo;

        public float XDelta;

        public Command Operation;
    }

    public enum Command
    {
        Sin = 1,
        Sqrt = 2,
        Cos = 3,
    }
}
