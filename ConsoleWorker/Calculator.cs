﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleWorker.Models;

namespace ConsoleWorker
{
    public class Calculator
    {
        private Message _msg;


        public Calculator(Message msg)
        {
            _msg = msg;
        }

        //тут напрашивается калькулятор обработки произвольных записей  (обратная польская запись)
        //но в рамках тестового задания его реализовывать будет довольно трудоемко, поэтому ограничимся забитыми примерами выражений 
        public List<float> CalcY()
        {
            List<float> list = new List<float>();
            switch (_msg.Operation)
            {
                //sin(x+5)
                case Command.Sin:
                    for (float x = _msg.XFrom; x <= _msg.XTo; x += _msg.XDelta)
                    {
                        float vx = (float)Math.Round(x+5, 4);
                        float y = (float)Math.Sin(vx);
                        if (y > 0)
                            list.Add(y);
                    }
                    break;
                //cos(x*2)
                case Command.Cos:
                    for (float x = _msg.XFrom; x <= _msg.XTo; x += _msg.XDelta)
                    {
                        float vx = (float)Math.Round(x*2, 4);
                        float y = (float)Math.Cos(vx);
                        if (y > 0)
                            list.Add(y);
                    }
                    break;
                //sqrt(6-x)
                case Command.Sqrt:
                    for (float x = _msg.XFrom; x <= _msg.XTo; x += _msg.XDelta)
                    {
                        float vx = (float)Math.Round(6-x, 4);
                        float y = (float)Math.Sqrt(vx);
                        if (y > 0)
                            list.Add(y);
                    }
                    break;
                default:
                    throw new NotImplementedException();
                    break;
            }
            return list;
        }

        public float CalcArea(List<float> listY)
        {
            return listY.Sum(y => y* _msg.XDelta);
        }
    }
}
